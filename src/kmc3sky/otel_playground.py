#!/usr/bin/python3

from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor

import logging
log = logging.getLogger()
log.info("moolog")

resource=Resource.create({
    "service.name": "kmc3sky"
})

trace.set_tracer_provider(TracerProvider(resource=resource))

exporter = OTLPSpanExporter(insecure=True, endpoint="http://localhost:4317")
processor =  BatchSpanProcessor(exporter)
trace.get_tracer_provider().add_span_processor(processor)

tracer = trace.get_tracer(__name__)

with tracer.start_as_current_span("TopLevel"):
    #with tracer.start_as_current_span("SubLevel"):
    print("Moo!")
