#!/usr/bin/python3

#
# KMC3 detectors: Pilatus, Eiger, Point detector.
#
# We also have a simulator, but we're trying to cover up that fact...
#

from ophyd import \
    AreaDetector, \
    SingleTrigger, \
    ImagePlugin, \
    SimDetector, \
    Component, \
    PilatusDetector, \
    ADComponent

class Kmc3SimDetector(SingleTrigger, SimDetector):
    image = Component(ImagePlugin, suffix="image1:")

class Kmc3EigerDetector(SingleTrigger, AreaDetector):
    #image = Component(ImagePlugin, suffix="image1:")
    image = ADComponent(ImagePlugin, suffix="image1:")    
