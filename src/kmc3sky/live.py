#!/usr/bin/python3

''' Namespace preparations for KMC3 live runs.

This is intended to be used as `from kmc3sky.live import *` and
allow the user to start using KMC3 equipment directly from
IPython. 
'''

from kmc3sky.mono import Kmc3Mono
from kmc3sky.detector import Kmc3SimDetector, Kmc3EigerDetector
from os import environ

from bluesky import RunEngine
import bluesky.plans as bp
from bluesky.callbacks.best_effort import BestEffortCallback

__all__ = [ "mono", "RE", "bp", "setup_camera", "read_image" ]

kmc3_prefix = environ.get('KMC3_PREFIX', 'KMC3:XPP:')

mono = Kmc3Mono(kmc3_prefix, name="mono")

try:
    sim_det = Kmc3SimDetector(kmc3_prefix+"SIMDET:", name="simDet")
    __all__.append("sim_det")
except TimeoutError as e:
    print(f'Sim detector failed: {e}')

try:
    real_det = Kmc3EigerDetector(kmc3_prefix+"EIGER2:", name="eiger")
    __all__.append("real_det")
except TimeoutError as e:
    print(f'Eiger detector failed: {e}')

RE = RunEngine({})
RE.subscribe(BestEffortCallback())


def setup_camera(det):
    det.cam.stage_sigs['image_mode'] = 'Single'
    det.cam.stage_sigs['num_images'] = 1
    det.cam.stage_sigs['acquire_time'] = 0.1
    det.cam.stage_sigs['acquire_period'] = 2.0
    

def read_image(det):
    '''
    Returns the last (current) image of `det`, propertly reshaped
    '''
    array_size = det.image.array_size.get()
    return det.image.array_data.get().reshape(
        (array_size.height, array_size.width))
