#!/usr/bin/python3

# Ophyd devices and setups of the custom monochromator

from ophyd import (
    EpicsSignal,
    EpicsSignalRO,
    PVPositioner,
    Device,
    Component
)

from ophyd.pseudopos import (
    PseudoPositioner,
    PseudoSingle,
    pseudo_position_argument,
    real_position_argument
)

class HuberMotor(PVPositioner):
    ''' Convenience class to wrap a Huber motor with variables exposed by Exhub.

    The Exhub IOC has a limited subset of the Epics motor records.
    '''
    setpoint    = Component(EpicsSignal,   'VAL')
    readback    = Component(EpicsSignalRO, 'RBV')
    done        = Component(EpicsSignalRO, 'DMOV')
    stop_signal = Component(EpicsSignal,   'STOP')


class Kmc3Mono(Device):

    # Are these photsphorus screens? They should be removed from here
    scr1  = Component(HuberMotor, 'scr1_')
    scr2  = Component(HuberMotor, 'scr2_')

    # These will need a more concerted movement pattern perhaps...
    m1y   = Component(HuberMotor, 'm1y_')    
    m1z   = Component(HuberMotor, 'm1z_')
    m1omg = Component(HuberMotor, 'm1omg_')

    m2y   = Component(HuberMotor, 'm2y_')
    m2z   = Component(HuberMotor, 'm2z_')
    m2omg = Component(HuberMotor, 'm2omg_')
    m2chi = Component(HuberMotor, 'm2chi_')
