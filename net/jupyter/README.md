Jupyter notebook setup for KMC3
==============================

Contains:

  - [Official Datascience Notebook](docker.io/jupyter/datascience-notebook)
  
  - [nx5d](https://gitlab.com/codedump2/nx5d) with `xrd` modules.
    If `/nx5d_src` is defined, the package is installed from there
	(`podman build -v /nx5d_src:...`).
    Container can be built as-is (`podman build -f ./Dockerfile`), in which case
    #it pulls
    

Uses variables:

  - `KMC3_JUPYTER_PASSWORD`: clear-text password for the jupyter notebook
