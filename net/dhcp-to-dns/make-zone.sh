#!/bin/bash

set -euo pipefail

# Repository to read (and write!) zone configuration from (and to).
# If write access is not possible, writing will fail (obviously),
# but this script will still go through the motions of fetching templates
# and generating proper zonefiles. The zonefiles will simply just not
# go anywhere else, from here... :-(
: ${REPO_REMOTE:=https://user:pass@gitlab.com/kmc3-xpp/ops/conf-zonefiles}

# The DHCP MySQL database to connect to. User and database name are always 'kea'
: ${KEA_MYSQL_HOST:=10.88.37.99}

# The password to use for connecting to the database. (No, this isn't the
# KMC3 operational password. You actually need to paste that through
# 'podman run ...').
: ${KEA_MYSQL_PASS:="seriously, just no"}

# Where to store the local copy of the conf-zonfiles repo. You shouldn't
# have to change this, but in case you really want to (e.g. for making
# sure checkouts/commits are handled outside of this script...)
: ${REPO_LOCAL_STORAGE:=/var/data/zonefiles}

# Cosmetic: user to appear in git commits
: ${REPO_USER_EMAIL:="not-available@kmc3.physik.uni-potsdam.de"}
: ${REPO_USER_NAME:="KMC3 Zone Generator"}

# Zonefile to use as template (typically ./zonefiles/i.kmc3.physik.uni-potsdam.de
# from git@gitlab.com:kmc3-xpp/conf-zonefiles)
: ${ZONE_TEMPLATE:="$REPO_LOCAL_STORAGE/zonefiles/i.kmc3.physik.uni-potsdam.de.template"}

# New zonefile to create
: ${ZONE_GENERATED:=${ZONE_TEMPLATE%.*}}

##
## You shouldn't have to touch anything below this point
##
HERE=$(cd $(dirname $0) && pwd)

## Where to

function checkout_repo()
{
    mkdir -p $(dirname "$REPO_LOCAL_STORAGE")

    if [ -d "$REPO_LOCAL_STORAGE" ]; then
        # The local storage already contains a checked-out repo.
        # Make sure it's up-to-date.
        echo "Checking repo status for $REPO_LOCAL_STORAGE"
        pushd "$REPO_LOCAL_STORAGE"

        local repo_status=$(git status)

        if [ ! -z "$(echo \"$repo_status\" | grep working\ tree\ clean)" ]; then
            echo "Working tree is clean"
        else
            echo "Updating: $REPO_LOCAL_STORAGE"
            git reset --hard
            git pull origin --rebase
        fi

        if [ ! -z "$(echo \"$repo_status\" | grep Your\ branch\ is\ ahead\ of )" ]; then
            echo "ACHTUNG, apparently there are unpushed changes in local repo."
            echo "You can still continue, but should probably fix that."
        fi

        popd

    else
        # New clean checkout
        echo "Cloning: $REPO_REMOTE"
        git clone --recurse-submodules "$REPO_REMOTE" "$REPO_LOCAL_STORAGE"
    fi
}


# Parses zones directly from KEA's mysql DB.
# Exports to ZONE_DHCP_HOSTS
function export_dhcp_hosts()
{
    export ZONE_DHCP_HOSTS=$(\
        echo 'select INET_NTOA(address), HEX(hwaddr), hostname from lease4; ' |  \
	mysql -D kea -h $KEA_MYSQL_HOST -u kea -p"$KEA_MYSQL_PASS" | \
	awk 'NR>1 {
            if ($3 != "") {
	        gsub(/[.]/, "", $3);
                print($3 "\t\tIN\tA\t" $1);
                print($2 "\tIN\tA\t" $1);
                print("");
            }
        }')
}

# Exports a valid SOA serial number (based on the current date)
# to the env var ZONE_SERIAL
function export_zone_serial()
{
    SERIAL_SUFFIX=$[$[$(date +%H)*4]+$[$(date +%M)/15]]
    export ZONE_SERIAL="$(date +%Y%m%d)""$SERIAL_SUFFIX"
}

# Exports zone from $ZONE_TEMPLATE to $ZONE_GENERATED,
# replacing all env-vars within (mostly ZONE_SERIAL and ZONE_HOSTS)
function zone_generate()
{
    echo "Exporting zone to: $ZONE_GENERATED"
    cat "$ZONE_TEMPLATE" | envsubst > "$ZONE_GENERATED"
}

# Tries to push data back into the repo (locally only)
function repo_commit_local()
{
    echo "Committing $ZONE_GENERATED to local repo"

    local repo_status=$(git status)

    if [ ! -z "$(echo \"$repo_status\" | grep working\ tree\ clean)" ]; then
        echo "Working tree is clean, no DHCP related DNS changes"
    else
        echo "Commiting changes"
        git add .
        git config --global user.email "$REPO_USER_EMAIL"
        git config --global user.name "$REPO_USER_NAME"
        git commit -m "Auto-generated hosts, `date`, `hostname`"
    fi
}

# Tries to push local changes upstream
function repo_push_origin()
{
    echo "Trying to push back to default remote"
    git push origin
}


checkout_repo

pushd "$REPO_LOCAL_STORAGE"
export_dhcp_hosts
export_zone_serial
zone_generate
repo_commit_local
repo_push_origin && echo "Zone generated in $ZONE_GENERATED, pushed to origin." \
    || echo "Pushing to origin failed (URL: $(git remote get-url --push origin))"
