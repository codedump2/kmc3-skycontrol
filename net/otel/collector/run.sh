#!/bin/bash

# Run a opentelemetry collector (test purposes)

HERE=$(cd $(dirname $0) && pwd)

mkdir -p "$HERE/scratch" && chmod 0777 "$HERE/scratch"

podman run -ti --rm \
       -v $HERE/config.yaml:/etc/otelcol-contrib/config.yaml \
       -v $HERE/scratch:/scratch \
       --net=host \
       docker.io/otel/opentelemetry-collector-contrib 

#        -p 4317:4317 \
#-p 8888:8888 \
#       -p 8889:8889 \
#       -p 4318:4318 \
#       -p 55679:55679 \
#       -p 13133:13133 \
#       -p 1888:1888 \
       
