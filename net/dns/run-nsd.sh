#!/bin/bash

set -euo pipefail

# Where NSD will store its runtime data (caches, PID, zone transfer temp files)
: ${NSD_RUNTIME_DIR:=/var/lib/nsd}



# Necessary directories
mkdir -p \
      ${NSD_RUNTIME_DIR} \
      ${NSD_RUNTIME_DIR}/pid \
      ${NSD_RUNTIME_DIR}/db \
      ${NSD_RUNTIME_DIR}/tmp
