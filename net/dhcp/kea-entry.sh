#!/bin/bash

set -euo pipefail

# kea-dhcpd.conf template requires KEA_DB_PASS and KEA_DB_HOST env vars

cat /data/kea-dhcpd.conf.template | envsubst > /data/kea-dhcpd.conf
kea-admin db-init mysql -u kea -p $KEA_DB_PASS -n kea -h $KEA_DB_HOST || :
kea-dhcp4 -c /data/kea-dhcpd.conf
