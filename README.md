KMC3 Automated Host Configuration
=================================

Various hosts and tasks at KMC3 are configured automatically via
[Ansible](https://www.ansible.com/).

This is the root of the Ansible configuration tree and contains
recipes and playbooks for all KMC3 hosts.

Bootstrapping the SkyControl Host Management
--------------------------------------------

For libvirt:

### Creating the virtual networks

The typical SkyControl setup uses two networks:

  - `ops`, the network for operative tasks, living off a zero-trust philosophy,
    and thus meant to be (capable to become) fully exposed to the internet
  
  - `instr`, the instrumental network, meant to host sensitive, from an IT
    security standpoint outdated, infected, or otherwise isolation worthy
	devices.

The easiest way of doing this is using the virt-manager GUI utility 
(File -> Connection Details -> Virtual Networks -> Create new). The `ops`
network is best created as:
  - NAT network
  - forward to any physical device
  - host IP address (e.g.) 172.16.58.253/21
  - no DHCP (SkyControl is supposed to serve its own DHCP)
  - libvirt network name `sky-ops`
  
For the `instr` network:
  - type "Isolated"
  - IPv4 e.g. 172.16.48.253/24
  - libvirt network name `sky-instr`.


Hosts currently managed
-----------------------
 
  - `pixtend`
  
How to configure an existing host
---------------------------------
  
How to extend configuration to a new host
-----------------------------------------
