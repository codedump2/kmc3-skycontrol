#!/bin/bash

## Builds the bluesky container:
## requires $bluesky_dpluser and $bluesky_dplpass

HERE=$(cd `dirname $0` && pwd)
SECRETS=$HERE/../../local/secrets/bluesky

podman build -f $HERE/Dockerfile \
       --secret id=bluesky_dpluser,src=$SECRETS/bluesky_dpluser \
       --secret id=bluesky_dplpass,src=$SECRETS/bluesky_dplpass \
       $@

