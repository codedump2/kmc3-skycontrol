Pilatus IOC Container Image
===========================

This is a container image bundling an
[EPICS Input-Output Controller (IOC)](https://epics.anl.gov/EpicsDocumentation/AppDevManuals/AppDevGuide/3.12BookFiles/AppDevGuide.book.html)
based on [areaDetector](https://areadetector.github.io/areaDetector/)
and its
[ADPilatus](https://areadetector.github.io/master/ADPilatus/pilatusDoc.html)
module.

ADPilatus apparently requires a
[camserver](https://media.dectris.com/220621-User_Manual-DECTRIS_PILATUS3.pdf)
service running. Apparently, camserver is an old beast, having been around
for decades. There are
[older versions](https://subversion.xray.aps.anl.gov/synApps/areaDetector/tags/R1-9-1/documentation/User_Manual-PILATUS-V1_1.pdf)
around which may or may not work.

This image was developed and tested on a
[Pilatus 100K](https://www.dectris.com/en/support/manuals-docs/pilatus3-r-for-laboratory/pilatus3-r-100k-a/)
detector, with the Pilauts `det` utility (i.e. camserver) version ???
from around.
It *might* should work on whatever your local `camserver` installation
is able to drive. With the exception of the included camserver simulator,
this depends mostly on areaDetector's ADPilatus driver. Give it a try
and let us know, so we can update the list of Pilatus drivers
[known to work](#camserver-versions-known-to-work).


What's in the box
-----------------

The Pilatus IOC container is based on the
[epics-toolbx](https://gitlab.com/codedump2/epics-toolbx) image
of the KMC3-XPP beamline from BESSY-II. As such, at the time
of this writing it contains:

  - EPICS, in a version 7.0 release
  - areaDetector, current HEAD at the time of build
  - numerous EPICS modules as a prerequisite for areaDetector
    (asyn, synApps, sscan, scnSeq, calc, autosave, busy, iocStats)
  - all areaDetector device modules which are a submodule of the
    areaDetector main GitHub repository; but only ADSimDetector
	and ADPilatus are typically set up and tested to work
  - GraphicsMagic and other areaDetector dependencies from current
    Fedora (zlib, graphics libraries for NetCDF, HDF5, JPEG, XML
	libraries, and some limited X11 libraries needed to compile
	areaDetector and/or EPICS modules)

Additionally, this image brings also:

  - a built-in camserver simulator / mock-up for testing,
    written in Bash
  - nmap netcat, for the [camserver mock-up]()
  - ImageMagick, to produce fake images for the camserver mock-up
  - entry point scripts and configuration management for
    running the ADPilatus and ADSimDetector IOCs (obviously),
	for synchronizing an image folder from a remote camserver computer
	via ssh (optionally), and for optionally starting a remote
	camserver if it is not already running.
	

Installation
------------


Configuration and deployment
----------------------------


Testing and simulation
-----------------------


Bugs & caveats
--------------

### Camserver versions known to work
