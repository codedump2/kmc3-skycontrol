#!/bin/bash

set -euo pipefail

##
## This is a mock version of a Pilatus camserver.
## It listens on port 41234 and answers to the following
## camserver commands (always by sending a predefined picture):
##
##   + Exposure ...
##   + ExtTrigger ...
##   - ExtMTrigger {file}
##   + ExtEnable {file}
##   + help {cmd}
##
##   - SetCu
##   - SetMo
##   - SetFe
##   - SetAg
##   - SetCr
##   - SetThreshold [[gain] threshold}] (getter/setter)
##   - SetEnergy
##   - K (interrupts exposure series)
##   - camcmd K
##   - RateCorrLUTDir {dir}
##   - ReadoutTime (read-only)
##   - THread {channel}
##   - ResetCam
##   - HeadString
##   - Exit
##   - Quit
##   - Df
##   - ExpEnd
##   - CamSetup
##   - Telemetry
##   - ResetModulePower
##   - Version
##   - ShowPID
##   - version (??)
##   - thread (??)
##
## Unclear on whether these are Camserver commands or something else:
##
##   - mkmask [im] [imout] low high
##   - maskimg [im]
##   - pixfill [im] value
##
## Non-existent commands (but mentioned in older Dectris documentation):
##
##   + menu (???)
##   + dcb_init
##
##   - servrf {val}
##   - trimchip {N}
##   - show setdac
##   - setvcmp {value}
##
## The mockup mimics setting/reading of the following camserver variables:
##
##   + ImgPath
##   + NImages
##   + Delay
##   + NExpFrame
##   + ExpTime
##   - SetRetriggerMode {0|1}
##   - GapFill {0|-1}
##   - SetAckInt {num}
##   - DebTime
##   + ExpPeriod
##   - LdFlatField {file}
##   - LdBadPixMap {file}
##   - Tau (undocumented?)
##
## Apparently in newer versions only:
## 
##   - MxSettings {key} {value}, with keys:
##     - Energy_range
##     - Detector_distance
##     - Detector_Voffset
##     - Beam_xy
##     - Beam_x
##     - Beam_y
##     - Flux
##     - Filter_transmission
##     - Start_angle
##     - Angle_increment
##     - Detector_2theta
##     - Polarization
##     - Alpha
##     - Kappa
##     - Phi
##     - Phi_increment
##     - Chi
##     - Chi_increment
##     - Omega
##     - Omega_increment
##     - Oscillation_axis
##     - N_oscillations
##     - Start_position
##     - Position_increment
##     - Shutter_time
##     - CBF_template_ﬁl

: ${IMG_WIDTH=487}
: ${IMG_HEIGHT=195}
: ${CAMSERVER_PORT=41234}
: ${CAMSERVER_RX_EOM='\r'} # should be \n for original camserver
: ${CAMSERVER_TX_EOM='\n'} # should be \0x18 or \030 for original camserver
: ${CAMSERVER_VERBOSE="no"}

## You shouldn't have to touch anything below this point
##

## Note to self: everything that goes through STDOUT, goes back to the client;
## everything that goes to STDERR, goes to the local screen / logfile.

HERE=$(cd $(dirname $0) && pwd)

# simulated variables
VAR_EXPT=1.0
VAR_NI=1
VAR_EXPP=1.0
VAR_DELAY=1.0
VAR_NEXPF=1
VAR_IMGPATH="$HERE"

cmd_index=0

function tx_reply() {
    log "<<< $@"
    echo -ne "$@""$CAMSERVER_TX_EOM" >&1
}

function log() {
    echo "$(date --rfc-3339=ns --utc): $@" >&2
}

function log_verbose() {
    if [ "$CAMSERVER_VERBOSE" == "yes" ]; then
	log $@
    fi
}

# generic function to set/get VAR_... variables.
function handle_variable() {
    local key=$1

    var=VAR_${key^^}
    if [ -z ${!var+x} ]; then
	return 1
    fi
    #echo "$var: ${!var}" >&2

    if [ -z ${2+x} ]; then
	log "Getting $key: $var -> ${!var}"
	tx_reply "${!var}"
    else
	local value=$2
	log "Setting $key: $var <- $value"
	eval ${var}="$value"
    fi
	
    return 0
}

function handle_invalid() {
    log "MOCK ERROR: There is no '$@'"
}

# mock menu statement
function camserver_menu() {
    tx_reply "This is a mock camserver. Bring your own menu."
}

function camserver_help() {
    tx_reply "No help on \"$@\", or anything else."
}

# "exposure [filename]": mock exposure for a single image with VAR_EXPTIME
function camserver_exposure() {
    log "Image: $1"
}

# "exttrigger [filename]": mock exposure of multiple triggered images
function camserver_extt() {
    log "Trigger: $1"
}

# "extenable [filename]": mock exposure of multipe images
# (VAR_NIMAGES? non-triggered?)
function camserver_exte() {
    log "Multi: $1 * $VAR_NI"
}

# does nothing in the mock version. just here to avoid error messages.
function camserver_dcb_init() {
    log "DCP init, apparently"
}

:| { ncat -l $CAMSERVER_PORT --keep-open -m 1 | \
	 while read input; do
	     log ">>> $input" >&2
	     stripped_input="$(echo -n "${input}" | sed "s/$CAMSERVER_RX_EOM//g")"
	     args=( $stripped_input )
	     cmd=${args[0]}
	     unset args[0]

	     # increment the global command index
	     cmd_index=$[${cmd_index}+1]

	     # At this point, 'cmd' is the first word, and 'args' is an array
	     # with all the parameters.
	     #
	     # Now the thing is: camserver accepts shorthands for all its commands,
	     # as long as they're unique (i.e. "expt", "expti", "exptim" instead of
	     # "exptime"). To support that, define our local VAR... in the shortest
	     # way possible (i.e. "VAR_EXPT"), and cycle through all possible
	     # variants (by removing one letter from the back) before declare any
	     # of the actions a fail.
	     
	     while [ ! -z "$cmd" ]; do

		 # Try executing a camserver_...() command first.
		 # If that fails, try getting/setting a variable.
		 if type camserver_${cmd,,} >/dev/null 2>&1; then
		     log "Executing: camserver_${cmd,,} ${args[@]}"
		     camserver_${cmd,,} ${args[@]}
		     break
		     
		 elif handle_variable $cmd ${args[@]}; then
		     break
		     
		 fi
		 
		 # ...otherwise remove one letter from the command
		 # (that's the first member of 'cmd') and try again
		 cmd="${cmd::-1}"
		 log_verbose "Next candidate: $cmd"
		 
	     done

	     # final fail, if we run out of 'cmd' letters.
	     [ -z "$cmd" ] && handle_invalid "${stripped_input}"
	     
	 done } >/dev/fd/0
