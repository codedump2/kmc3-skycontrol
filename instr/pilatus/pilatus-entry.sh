#!/bin/bash

set -euo pipefail

##
## Entry point for the Pilatus detector IOC; uses areaDetector, needs
## to write /opt/areaDetector/.../st.cmd IOC "configuration file".
## See PILATUS_PATH and SIMDET_PATH for configuration.
##

##
## We're also offering several test modes for integration testing,
## controlled by the DETECTOR_TEST environment variable:
##
##   - "sim" starts ADSimDetector with parameters as similar
##     to ADPilatus as possible,
##
##   - "mock" runs a local mock camserver (mock-server.sh),
##     manipulates CAMSERVER_HOST to point to that, and
##     ignores the SSH settings. (Note that if you want to
##     run a remote mock-camserver, you should do that
##     independently from this script and just treat it as if
##     it were a real camserver as far as this script is
##     concerned).
##

## Configuration options; most don't need changing (careful if you do,
## make sure to know what you're doing! This isn't for the faint of heart.)

# Pilatus geometry
: ${IMG_WIDTH:=487}
: ${IMG_HEIGHT:=195}

# Prefix of all the EPICS variables
: ${EPICS_PREFIX:=KMC3:XPP:pilatus:}

# Connection to the Pilatus camserver host
: ${CAMSERVER_HOST:=172.16.58.162}
: ${CAMSERVER_PORT:=41234}

# If camserver is not already started on the remote host, and this
# variable is set to something non-empty, then we SSH into the remote
# host first and start a camserver there.
# This is actually the preferred method (as opposed to having the
# camserver run there), since we can capture the camserver's output
# and log it accordingly.
# You should only think about overriding this if you have problems
# with remote camserver binaries (i.e. non-standard statup script).
: ${CAMSERVER_SSH_USER:=det}

# SSH command to execute when starting camserver on a remote machine.
# Needs to include all and any exotic options you may want to pass
# to SSH. Hint: you need at least an '-i ...' parameter for specifying
# the private key to use (~/.ssh/camserver will be pasted for you if
# CAMSERVER_SSH_IDENTITY is set), and you may need some obscure cypher
# settings if your camserver is old and doesn't support a modern SSH
# cypher suite.
: ${CAMSERVER_SSH_COMMAND:="ssh -i ~/.ssh/camserver $CAMSERVER_SSH_USER@$CAMSERVER_HOST"}

# Where on the remote server is the Pilatus suite located (this is
# the location of the base directory -- the one which contains the
# "camrc" file).
: ${CAMSERVER_SSH_PILATUS_PATH:=""}

# base64 encoded SSH private key. If this is non-empty, it will be pasted
# into ~/.ssh/camserver of the executing user.
: ${CAMSERVER_SSH_IDENTITY:=""}

# Image path on the camserver host
: ${CAMSERVER_IMAGE_PATH:=/home/data/disk_2/tmp}

# Workdir defaults to jinkies home folder
: ${IOC_WORKDIR:=/home/jinkies}

# Set to "sim" or "mock" for testing (see above).
: ${DETECTOR_TEST:=no}

# Folder where the mock detector puts its files.
# We like to keep this separated from the CAMSERVER_IMAGE_PATH
# settings for now.
: ${CAMSERVER_MOCK_IMAGES:=/tmp}

# Temporary locations for mock-server log file
: ${CAMSERVER_MOCK_LOGFILE:=/tmp/mock-camserver.log}


##
## You shouldn't have to change anything below this point, unless
## you're substantially deviating from the norm (i.e. use another
## base image, try to develop your own pilatus entry script,
## or generally are just here to rip things apart and watch the
## world burn... ¯\_(ツ)_/¯
##

## EPICS/IOC relevant files within the base image
AREADETECTOR_PATH="/opt/areaDetector"
PILATUS_PATH="$AREADETECTOR_PATH/ADPilatus/iocs/pilatusIOC/iocBoot/iocPilatus"
SIMDET_PATH="$AREADETECTOR_PATH/ADSimDetector/iocs/simDetectorIOC/iocBoot/iocSimDetector"

function start_simdet_ioc()
{
    cd $IOC_WORKDIR

    cat > st.cmd <<EOF
< envPaths
rrlogInit(20000)
dbLoadDatabase("\$(TOP)/dbd/simDetectorApp.dbd")
simDetectorApp_registerRecordDeviceDriver(pdbbase)                                                               
epicsEnvSet("PREFIX", "$EPICS_PREFIX")
epicsEnvSet("PORT",   "SIM1")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "$IMG_WIDTH")
epicsEnvSet("YSIZE",  "$IMG_HEIGHT")
epicsEnvSet("NCHANS", "2048")
epicsEnvSet("CBUFFS", "500")                                                                                     
epicsEnvSet("MAX_THREADS", "8")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "\$(ADCORE)/db")
asynSetMinTimerPeriod(0.001)
simDetectorConfig("\$(PORT)", \$(XSIZE), \$(YSIZE), 1, 0, 0)
# dbLoadRecords("simDetector.template","P=\$(PREFIX),R=cam1:,PORT=\$(PORT),ADDR=0,TIMEOUT=1,RATE_SMOOTH=0.2")
dbLoadRecords("\$(ADSIMDETECTOR)/db/simDetector.template","P=\$(PREFIX),R=cam1:,PORT=\$(PORT),ADDR=0,TIMEOUT=1")
NDStdArraysConfigure("Image1", 20, 0, "\$(PORT)", 0, 0, 0, 0, 0, 5)
dbLoadRecords("NDStdArrays.template", "P=\$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=\$(PORT),TYPE=Int8,FTVL=UCHAR,NELEMENTS=12000000")
< \$(ADCORE)/iocBoot/commonPlugins.cmd
set_requestfile_path("\$(ADSIMDETECTOR)/simDetectorApp/Db")
# asynSetTraceIOMask("\$(PORT)",0,2)
iocInit()
EOF

    cp $SIMDET_PATH/envPaths.linux $IOC_WORKDIR/envPaths

    $SIMDET_PATH/../../bin/linux-x86_64/simDetectorApp st.cmd
}

function start_pilatus_ioc()
{
    cd $IOC_WORKDIR

    cat > st.cmd <<EOF
< envPaths
errlogInit(20000)
dbLoadDatabase("\$(TOP)/dbd/pilatusDetectorApp.dbd")
pilatusDetectorApp_registerRecordDeviceDriver(pdbbase)
epicsEnvSet("PREFIX", "$EPICS_PREFIX")
epicsEnvSet("PORT",   "PIL")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "$IMG_WIDTH")
epicsEnvSet("YSIZE",  "$IMG_HEIGHT")
epicsEnvSet("NCHANS", "2048")
epicsEnvSet("CBUFFS", "500")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "\$(ADCORE)/db")
drvAsynIPPortConfigure("camserver","$CAMSERVER_HOST:$CAMSERVER_PORT")
# asynSetTraceIOMask("camserver",0,2)
# asynSetTraceMask("camserver",0,9)
asynOctetSetInputEos("camserver", 0, "\\030")
asynOctetSetOutputEos("camserver", 0, "\\n")
pilatusDetectorConfig("\$(PORT)", "camserver", \$(XSIZE), \$(YSIZE), 0, 0)
dbLoadRecords("\$(ADPILATUS)/db/pilatus.template","P=\$(PREFIX),R=cam1:,PORT=\$(PORT),ADDR=0,TIMEOUT=1,CAMSERVER_PORT=camserver")
NDStdArraysConfigure("Image1", 5, 0, "\$(PORT)", 0, 0)
dbLoadRecords("\$(ADCORE)/db/NDStdArrays.template","P=\$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=\$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=94965")
< \$(ADCORE)/iocBoot/commonPlugins.cmd
set_requestfile_path("\$(ADPILATUS)/pilatusApp/Db")
# asynSetTraceMask("\$(PORT)",0,255)
iocInit()

iocshCmd("dbl")

iocshCmd("dbpf ${EPICS_PREFIX}cam1:FilePath ${CAMSERVER_IMAGE_PATH}")
iocshCmd("dbpf ${EPICS_PREFIX}cam1:FileTemplate jinkies_.tif")
iocshCmd("dbpf ${EPICS_PREFIX}cam1:FileName jinkies.tif")
iocshCmd("dbpf ${EPICS_PREFIX}cam1:FullFileName ${CAMSERVER_IMAGE_PATH}/scooby.tif")
iocshCmd("dbgf ${EPICS_PREFIX}cam1:FilePath")
iocshCmd("dbgf ${EPICS_PREFIX}cam1:FileTemplate")

EOF

    cp $PILATUS_PATH/envPaths $IOC_WORKDIR
    $PILATUS_PATH/../../bin/linux-x86_64/pilatusDetectorApp st.cmd
}


if [ "$DETECTOR_TEST" == "sim" ]; then

    start_simdet_ioc()
    
elif [ "$DETECTOR_TEST" == "mock" ]; then
    # mock-server will require two things:
    #
    #  (1) starting a mock server in the first place
    #
    #  (2) some alterations to variables, mainly to
    #      make sure we actually connect to localhost
    #      and look for the data in the right spot
    #

    # (2)
    echo "Overriding \$CAMSERVER_HOST to \"localhost\""
    CAMSERVER_HOST=localhost

    echo "Overriding \$CAMSERVER_IMAGE_PATH to \"$CAMSERVER_MOCK_IMAGES\""
    CAMSERVER_IMAGE_PATH="$CAMSERVER_MOCK_IMAGES"

    # (1)
    echo Stating mock-camserver
    CAMSERVER_TX_EOM="\n" CAMSERVER_RX_EOM="\0x18" \
    mock-camserver.sh > "$CAMSERVER_MOCK_LOGFILE" 2>&1 &
    
    sleep 1

    echo Starting Pilatus IOC
    start_pilatus_ioc
    
else
    # We may or may not have to SSH into the camserver machine here
    # and start the actual camserver. We do that depending on whether
    # we have a CAMSERVER_SSH_URL is empty or not.

    #if [ "$
    
    start_pilatus_ioc
fi
