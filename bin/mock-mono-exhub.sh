#!/bin/bash

scr1="-10:10"
scr2="-10:10"
m1y="-10:10"
m1z="-10:10"
m1omg="-10:10"
m2y="-10:10"
m2z="-10:10"
m2omg="-10:10"
m2chi="-10:10"

MOCK=""

for m in scr1 scr2 m1y m1z m1omg m2y m2z m2omg m2chi; do
    [ "foo$MOCK" != "foo" ] && MOCK="$MOCK;"
    MOCK="$MOCK$m:${!m}"
done

EXHUB_PREFIX=KMC3:sim: \
    EXHUB_IOC_TEST=yes \
    EXHUB_MOCK_HUBER="$MOCK" \
    exhub-ioc
