FROM registry.fedoraproject.org/fedora:36

RUN mkdir -p /opt/bluesky &&  \
    adduser -u 1000 bluesky

RUN dnf -y install git python3.9 nano && \
    mkdir -p /opt/bluesky/python3.9 net-tools iproute && \
    python3.9 -m venv /opt/bluesky/python3.9

RUN source /opt/bluesky/python3.9/bin/activate && \
    python3.9 -m ensurepip --upgrade && \
    python3.9 -m pip install \
    bluesky \
    ophyd \
    databroker \
    matplotlib \
    ipython \
    pyepics \
    scipy \
    scikit-image \
    wheel \
    Levenshtein \
    lmfit \
    PyQt5 \
    bluesky-live \
    dataclasses  \
    h5py \
    tables

ENV PYTHONENV=/opt/bluesky/python3.9 

# Copying bluesky source as they are
RUN --mount=type=secret,id=bluesky_dpluser \
    --mount=type=secret,id=bluesky_dplpass \
    source $PYTHONENV/bin/activate && \
    export BSUSER=`cat /run/secrets/bluesky_dpluser` && \
    export BSPASS=`cat /run/secrets/bluesky_dplpass` && \
    export BSGITBASE=https://$BSUSER:$BSPASS@gitlab.helmholtz-berlin.de/bessyII/bluesky && \
    echo Have URL: $BSGITBASE && \
    \
    mkdir -p /tmp/scratch  \
    && \
    for project in bessyii bessyii_devices suitcase-specfile; do \
        cd /tmp/scratch &&\
        git clone --recurse-submodules --depth=1 $BSGITBASE/$project &&\
        cd $project && \
        [ -f setup.py ] && python3.9 -m pip install . ;\
    done && \
    cd /opt/bluesky && \
    git clone --recurse-submodules --depth=1 $BSGITBASE/shell_scripts

# ...special cases
RUN --mount=type=secret,id=bluesky_dpluser  \
    --mount=type=secret,id=bluesky_dplpass  \
    source $PYTHONENV/bin/activate && \
    export BSUSER=`cat /run/secrets/bluesky_dpluser` && \
    export BSPASS=`cat /run/secrets/bluesky_dplpass` && \
    export BSGITBASE=https://$BSUSER:$BSPASS@gitlab.helmholtz-berlin.de/bessyII/bluesky && \
    echo Have URL: $BSGITBASE && \
    \
    cd /tmp/scratch && \
    git clone --recurse-submodules --depth=1 $BSGITBASE/XPP-KMC3/beamlinetools &&\
    cd beamlinetools && \
    python3.9 -m pip install . && \
    \
    cd /opt/bluesky &&\
    git clone --recurse-submodules --depth=1 $BSGITBASE/XPP-KMC3/profile_root kmc3

RUN \
    rm -rf /tmp/scratch

ENV PATH=$PATH:/opt/bluesky/shell_scripts/bin
ENV PYTHONPATH=$PYTHONPATH:/opt/bluesky

# RE(scan([det1],motor1,1,2,10))
