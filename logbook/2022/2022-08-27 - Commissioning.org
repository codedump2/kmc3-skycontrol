* KMC-3 beamline commissioning logbook

** 2023-01-14 Commissioning for SB: Optimize M1 and M2 for SB conditions

** 2023-01-09 Commissioning: Realign M1 and M2 as the energy resolution for EXAFS is bad at Cu edge


** 2022-09-05 X-ray spotsize again
*** solve the mystery of the visibility of the contacts on PZT
  (resolve small structures) and optimize the X-ray spotsize:
  - [ ] develop a common protocol for beamline alignment
  - [ ] check camera orientation and x- as well as y-orientation
  - [ ] check focus homogeneity with slits
  - [ ] check that changes of the mirror has using the camera image
    have the desired impact and cross-check this on the sample Bragg
    reflection; requires the next working point:
  - [-] construct a holder to mount the X-ray camera behind the sample
    meaning that the sample has not to be removed for X-ray spotsize
    determination.
    Space constraints: real parallel operation might not
    be possible as the camera will protrude to far to the back and the
    sample stage motors might be in the way. But for a quick test it might
    be okay.
  - [X] check slit settings (pinhole) for October measurement where we
    saw the electrodes nicely.
    These measurements were done with the 100 um pinhole. 

** 2022-09-02 X-ray spotsize
- check X-ray spotsize using a "knife edge": beam seems to be
  horizontally 150 - 200 um large, vertically almost 1 mm
  (camera told during commissioning ~30 px x ~70 px -> 150 um x 350 um)
- check spotsize with X-ray camera at sample position; confirmed the values
- playing with M2 pitch reduced the beamsize to vertically 40 px
- 

** 2022-08-30  Absorber box removed again
- Apparently vacuum lube was a bad idea, most absorbers not working
- Removed again, dismantled, cleanded rails and bearing slides
  in ultrasound bath (12 minutes water + soap, ~8 minutes isopropanol)
- Isopropanol ultrasound does the trick, absorber working well
  (except no. 3, sometimes still stuck; rails too old possibly)

** 2022-08-27  Absorber box removed & cleaned 
- Removed from mounting
- Cleaned bearings and rails with WD-40 and
- Lubrication: vacuum lube
- Re-attached, removed one spare absorber slide (not enough bearing balls)
  
