#!/bin/bash

# This only works at HZB

set -euo pipefail

BASE=$(dirname $0)
HERE=$(cd $BASE && pwd)
echo "Target dir: $HERE"

scp admin@iego:./flash/daily-backup.backup $HERE
scp admin@iego:./flash/daily-backup.text.rsc $HERE
