# oct/11/2024 13:58:10 by RouterOS 6.49.17
# software id = FRQW-BFYX
#
# model = CRS326-24G-2S+
# serial number = HE308KXKKDQ
/interface bridge
add frame-types=admit-only-vlan-tagged name=oih-vlan vlan-filtering=yes
add admin-mac=48:A9:8A:4B:AE:90 auto-mac=no name=ops-bridge
/interface ethernet
set [ find default-name=ether23 ] name=ether23-admin
set [ find default-name=ether24 ] name=ether24-uplink
/interface list
add name=WAN
add name=LAN
add name=ops-net
add name=oih-trunk
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/interface bridge port
add bridge=ops-bridge interface=ether9
add bridge=ops-bridge interface=ether10
add bridge=ops-bridge interface=ether11
add bridge=ops-bridge interface=ether12
add bridge=ops-bridge interface=ether13
add bridge=ops-bridge interface=ether14
add bridge=ops-bridge interface=ether15
add bridge=ops-bridge interface=ether16
add bridge=ops-bridge frame-types=admit-only-vlan-tagged interface=ether3
add bridge=ops-bridge frame-types=admit-only-vlan-tagged interface=ether4
add bridge=ops-bridge frame-types=admit-only-vlan-tagged interface=ether5
add bridge=ops-bridge frame-types=admit-only-vlan-tagged interface=ether6
add bridge=ops-bridge frame-types=admit-only-vlan-tagged interface=ether7
add bridge=ops-bridge frame-types=admit-only-vlan-tagged interface=ether8
add bridge=ops-bridge interface=sfp-sfpplus1
add bridge=ops-bridge interface=sfp-sfpplus2
add bridge=oih-vlan frame-types=admit-only-untagged-and-priority-tagged \
    interface=ether18 pvid=58
add bridge=oih-vlan frame-types=admit-only-untagged-and-priority-tagged \
    interface=ether20 pvid=49
add bridge=oih-vlan frame-types=admit-only-untagged-and-priority-tagged \
    interface=ether22 pvid=800
add bridge=oih-vlan frame-types=admit-only-vlan-tagged interface=oih-trunk
/interface bridge vlan
# port with pvid added to untagged group which might cause problems, consider adding a seperate VLAN entry
add bridge=oih-vlan tagged=ether17,ether19,ether21 vlan-ids=58,49,800
/interface list member
add interface=ops-bridge list=LAN
add list=WAN
add interface=ether8 list=ops-net
add interface=ether9 list=ops-net
add interface=ether10 list=ops-net
add interface=ether11 list=ops-net
add interface=ether12 list=ops-net
add interface=ether13 list=ops-net
add interface=ether14 list=ops-net
add interface=ether15 list=ops-net
add interface=ether16 list=ops-net
add interface=ether17 list=ops-net
add interface=ether18 list=ops-net
add interface=ether19 list=ops-net
add interface=ether20 list=ops-net
add interface=ether21 list=ops-net
add interface=ether22 list=ops-net
add interface=ether17 list=oih-trunk
add interface=ether19 list=oih-trunk
add interface=ether21 list=oih-trunk
/ip address
add address=172.16.56.117/21 interface=ops-bridge network=172.16.56.0
add address=192.168.88.1/24 interface=ether23-admin network=192.168.88.0
add address=172.31.200.117/24 interface=ether24-uplink network=172.31.200.0
/ip dhcp-client
add disabled=no
add disabled=no interface=oih-vlan
add disabled=no
/ip dns
set servers=172.31.200.12,172.31.200.13
/ip firewall filter
add action=reject chain=forward port=67,68 protocol=udp
/ip firewall nat
add action=masquerade chain=srcnat out-interface-list=WAN
/ip proxy
set parent-proxy=172.31.200.15 parent-proxy-port=3128
/ip route
add distance=1 gateway=172.16.58.147
/ip service
set telnet disabled=yes
set ftp disabled=yes
set www disabled=yes
set api disabled=yes
set winbox disabled=yes
set api-ssl disabled=yes
/ip ssh
set always-allow-password-login=yes strong-crypto=yes
/system clock
set time-zone-name=Europe/Berlin
/system identity
set name=iego
/system routerboard settings
set boot-os=router-os
/system scheduler
add interval=23h59m34s name=auto-update on-event=auto-update policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon \
    start-date=sep/28/2024 start-time=06:37:41
add interval=1d name=daily-backup on-event=daily-backup policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon \
    start-date=oct/12/2024 start-time=06:43:01
/system script
add dont-require-permissions=no name=auto-update owner=admin policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon source="/\
    system package update\
    \ncheck-for-updates once\
    \n:delay 3s;\
    \n:if ( [get status] = \"New version is available\") do={ install }\
    \n"
add dont-require-permissions=no name=daily-backup owner=admin policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon source="/\
    system backup save name=flash/daily-backup.backup\
    \n/export file=flash/daily-backup.text"
