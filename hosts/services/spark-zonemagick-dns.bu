variant: fcos
version: 1.5.0

storage:

  directories:
    
  files:

    # Requires /etc/spark.sh/zonemagick.env with the following env vars:
    #
    #  - ZMA_DNS_CONFIG_IMAGE: configuration image URL

    - path: /etc/containers/systemd/zonemagick-dns-config.container
      mode: 0644
      contents:
        inline: |
          [Install]
          WantedBy=default.target

          [Service]
          TimeoutStartSec=infinity
          EnvironmentFile=/etc/spark.sh/zonemagick.env
          Restart=always
          RestartSec=3

          [Unit]
          Description=ZoneMagick DNS configuration bootstrap
          After=network-online.target
          StartLimitBurst=10
          StartLimitIntervalSec=9
          StopWhenUnneeded=true
          Upholds=zonemagick-dns.service

          [Container]
          ContainerName=zonemagick-dns-config
          AutoUpdate=registry
          Image=$ZMA_DNS_CONFIG_IMAGE
          Volume=/etc/nsd
          Exec=sh -c "while true; do sleep 1492m; done"
          Pull=always


    - path: /etc/containers/systemd/zonemagick-dns.container
      mode: 0644
      contents:
        inline: |
          [Install]
          WantedBy=default.target

          [Service]
          TimeoutStartSec=infinity
          EnvironmentFile=/etc/spark.sh/zonemagick.env
          Restart=always
          RestartSec=3

          [Unit]
          Description=ZoneMagick DNS service
          After=network-online.target
          After=zonemagick-dns-config.service
          BindsTo=zonemagick-dns-config.service
          StartLimitBurst=10
          StartLimitIntervalSec=9
          PropagateStopTo=zonemagick-dns.service

          [Container]
          AutoUpdate=registry
          Image=registry.gitlab.com/kmc3-xpp/zonemagick:dns
          ContainerName=zonemagick-dns
          PodmanArgs=--volumes-from zonemagick-dns-config
          Volume=zonemagick-dns-runtime-volume:/var/lib/nsd
          Pull=always
