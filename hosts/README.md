How to test hosts with libvirt
==============================

You need:

  - lib-virt installed, along with other kvm/qemu packages required
    for your architecture and distribution, e.g. for Fedora Silverblue:
	```
	rpm-ostree libvirt libvirt-daemon-kvm libvirt-daemon-qemu qemu-kvm tmux virt-install
	```

  - a Fedora CoreOS image, preferrably in `.qcow2` format, under
    `/var/lib/libvirt/images`:
	```
	xz -d ~/Downloads/fedora-coreos-*.qcow2.xz
	mv ~/Dodloads/fedora-coreos-*.qcow2 /var/lib/libvirt/images
	```
	
	
	
Steps to run:

  - download this repo to your disk:
    ```
	git clone https://gitlab.com/kmc3-xpp/skycontrol
	```
	
  - edit/create `.ign` file for your favourite host:
    ```
	cd skycontrol/hosts
	./ignite.sh darkstar
	```
	
  - start virtual machine as a privileged user:
    ```
	sudo su
	./virt-run.sh darkstar
	```
	
	Note that the `virt-run.sh` script expects a Fedora CoreOS image
	in `/var/lib/libvirt/images`.
	
  - when finished, destroy the VM with `virsh undefine <hostname>-test`.
