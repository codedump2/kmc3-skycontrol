#!/bin/bash

# Reads Pilatus cam / image data from 10.0.0.114:/home/data/disk_2
# and saves it into /mnt/pool/XPP-measurement.
#
# ACHTUNG, there are a number of caveats:
#
#   - The data pool is huge, about 1.2 TB; we can't copy all of that.
#
#   - The format there is different from the storage format here:
#     There the image files are in: YYYY/NNN-MMMMM-name/pilatus/S00001-00999/S00001/...
#     Here the image files are in:  YYYY/NNN-MMMMM-name/pilatus/S00001/...
#     In other words, one directory component is skipped / flattened.
#
#   - Some data is compressed (past years -- ignore that?)
#
#   - OpenSSH version is old and only supports RSA keys.
#
# So in order to make a good tradeoff between time, completeness and
# backwards compatibility with the way files have been saved until now,
# we will:
#
#   - Use an RSA key only (obviously)
#
#   - Skip the S00001-00999 directory component (i.e. flatten all the scans)
#
#   - Ignore everything except the current year, unless the current year
#     doesn't yet exist locally (in which case we must assume it's January
#     and the last year wasn't yet completely synchronized)
#
#   - Ignore everything except the most recent grant (i.e. the NNN-MMMMM part),
#     unless the most recent grant doesn't already exist locally (in which
#     case we must assume that the previous grant might have not been
#     sync'ed fully and we'll take that first).
#
#   - Ignore everything except the most recent scan, unless the most recent
#     scan doesn't yet exist locally (in which case we assume that the previous
#     scan hasn't been yet sync'ed fully and we must take that first)

set -euo pipefail

# Lockfile -- if this file exists, this script will not run
CAMSYNC_LOCK=/tmp/ancient_camlock

# How to call SSH on the cam server
CAMSYNC_SSH=${CAMSYNG_SSH="ssh -i ~/.ssh/for_ancient_camserver"}

# Which cam server
CAMSYNC_LOGIN=specuser@10.0.0.114

# Where the data is stored on the cam server
CAMSYNC_CAM_STORAGE=${CAMSYNC_CAM_STORAGE=/home/data/disk_2}

# Where the data is stored, here
CAMSYNC_LOCAL_STORAGE=${CAMSYNC_LOCAL_STORAGE=/mnt/pool/XPP-measurement}

# rsync command
CAMSYNC_RSYNC=${CAMSYNC_RSYNC="rsync -rvltDP"}


## Configuration ends here -- don't touch unless you want to change
## how things work.

# Shortcut for SSH command
CAMSSH="$CAMSYNC_SSH $CAMSYNC_LOGIN"

# Don't touch this.
OWN_LOCK=/usr/bin/false

#
# Prints a list of years (on camserver) that likely require syncing.
#
guess_recent_years()
{
    local dirs=`$CAMSSH find  "$CAMSYNC_CAM_STORAGE"/ \
        -name ???? \
        -maxdepth 1 \
        -type d | sort -rn `

    local years=""

    for d in $dirs; do
        year="${d##*/}"
        years="$year $years"
        [ -d "$CAMSYNC_LOCAL_STORAGE/$year" ] && break
    done

    echo $years
}

#
# Prints a list of grant-names to check for a specified year ($1)
#
guess_recent_proposals()
{
    local year="$1"
    [ "foo$year" == "foo" ] && echo guess_recent_proposal: need year && exit -1

    local dirs=`$CAMSSH find  "$CAMSYNC_CAM_STORAGE/$year" \
        -maxdepth 1 \
        -mindepth 1 \
        -type d | sort -r `

    local proposals=""

    for d in $dirs; do
        prop=${d##*/}
        proposals="$prop $proposals"
        [ -d "$CAMSYNC_LOCAL_STORAGE/$year/$prop" ] && break
    done

    echo $proposals

}

#
# Finds the most recent set of scans within year ($1) and proposal ($2)
# Prints a list of scan names here and their counterpart on the camserver
# (e.g. "S00001=S00001-00999").
#
guess_recent_pilatus_scans()
{
    local year="$1"
    local prop="$2"
    [ "foo$year" == "foo" ] && echo $0: need year and proposal && exit -1
    [ "foo$prop" == "foo" ] && echo $0: need yera and proposal && exit -1

    local dirs=`$CAMSSH find  "$CAMSYNC_CAM_STORAGE/$year/$prop/pilatus/" \
        -name S\* \
        -maxdepth 2 \
        -mindepth 2 \
        -type d | sort -rn`

    local scans=""

    for d in $dirs; do
	sc_here=${d##*/}
        sc_dirname=${d%/*}
        sc_there=${sc_dirname##*/}/$sc_here
        sc_pair="pilatus/$sc_here=pilatus/$sc_there"
        #echo $sc_pair
        scans="$sc_pair $scans"
        [ -d "$CAMSYNC_LOCAL_STORAGE/$year/$prop/pilatus/$sc_here" ] && break
    done

    echo $scans
}

# Besides the main scan data, Pilatus sometimes creates more files
# for several types of commands. These do not match the "Sxxxx"-structure.
# They lie there in the "$year/pilatus/..." folder instead.
# This prints out all the compontents within "pilatus/..." that are NOT
# "Sxxxx" and likely need to be synced.
#
# Expects year ($1) and proposal name ($2)
guess_pilatus_nonscans()
{
    local year="$1"
    local prop="$2"
    [ "foo$year" == "foo" ] && echo $0: need year and proposal && exit -1
    [ "foo$prop" == "foo" ] && echo $0: need yera and proposal && exit -1

    local dirs=`$CAMSSH find  "$CAMSYNC_CAM_STORAGE/$year/$prop/pilatus/" \
        -not -name S\* \
        -maxdepth 1 \
        -mindepth 1`

    for d in $dirs; do
        # There's no way how we can predict which of these
        # contain new data, because they're not scans that
        # increase monotonically. So we just print all of them.
        # Keep the $here=$there syntax.
        extra="${d##*/}"
        echo "pilatus/$extra=pilatus/$extra"
    done
}

# Catch-all list for non-pilatus data 
guess_nonpilatus()
{
    local year="$1"
    local prop="$2"
    [ "foo$year" == "foo" ] && echo $0: need year and proposal && exit -1
    [ "foo$prop" == "foo" ] && echo $0: need yera and proposal && exit -1

    local dirs=`$CAMSSH find  "$CAMSYNC_CAM_STORAGE/$year/$prop" \
        -not -name pilatus \
        -maxdepth 1 \
        -mindepth 1`

    for d in $dirs; do
        extra="${d##*/}"
        echo "$extra=$extra"
    done
}

#
# Wrapper to call the rsync command. Inputs are THERE ($1) and HERE ($2),
# with:
#   - HERE being the local path after after $CAMSYNC_LOCAL_STORAGE/$year/$proposal,
#   - THERE being the remote path after $CAMSYNC_CAM_STORAGE/$year/$proposal
#
do_sync()
{
    local here="$2"
    local there="$1"

    local year="$3"
    local proposal="$4"

    [ "foo$there" == "foo" ] && echo $0: need remote dir stub && exit -1
    [ "foo$here" == "foo" ] && echo $0: need local dir stub && exit -1
    [ "foo$year" == "foo" ] && echo $0: need year path component && exit -1
    [ "foo$here" == "foo" ] && echo $0: need propsal path component && exit -1

    mkdir -p "$CAMSYNC_LOCAL_STORAGE/$year/$proposal/$here/"

    RSYNC_RSH="$CAMSYNC_SSH" $CAMSYNC_RSYNC \
        "$CAMSYNC_LOGIN:$CAMSYNC_CAM_STORAGE/$year/$proposal/$there/" \
                     "$CAMSYNC_LOCAL_STORAGE/$year/$proposal/$here/"

}

#
# Exit handler to clean up lock.
#
cleanup()
{
    $OWN_LOCK && rm -f $CAMSYNC_LOCK
}


## This is where the real work begins. Above this point it's all
## just declarations & setup, below this point is the "do stuff"
## part of the script.

# First thing we want to do: prevent this script from running in
# parallel more than one instance.
#
# Oh, and by the way: if you ever feel the need to write any procedural
# commands above this point DO. NOT. DO. THAT! or you will fuck up
# locking, and I will come and haunt you in your sleep.
#
# If you don't understand what that means, you have no business editing
# this script.


trap cleanup EXIT SIGINT SIGKILL

[ -e $CAMSYNC_LOCK ] \
    && echo "$0: lockfile $CAMSYNC_LOCK indicates a running instance. Bye." \
    && exit -1

if ! touch $CAMSYNC_LOCK; then
    echo "$0: cannot create lockfile $CAMSYNC_LOCK"
    exit -1
fi

# From here on, it's only us
OWN_LOCK=/usr/bin/true

# We do through all the years likely to contain sync data
# (i.e. those that aren't on this server already), then
# for each year through all proposal names likely to contain
# data (those that aren't here already), and then through all
# scans. This works because, and only if, years, proposals
# and scan folders have a chronologically increasing name.

years=$(guess_recent_years)

for y in $years; do
    echo "CAMSYNC: + Year $y"
    props=$(guess_recent_proposals $y)

    for p in $props; do
        echo "CAMSYNC:  + Proposal $y/$p"

        # Main data: scan images.
        scans=$(guess_recent_pilatus_scans $y $p)
      
        # Courtesy to the confused user: non-scan data.
        # Note that this will likely be checked every
        # time, so you _really_ want to keep them small
        # or fuck up sync peformance.
        nonscans=""
        extra=""

	# Uncomment this to enable nonscans and extra folders.
        #nonscans=$(guess_pilatus_nonscans $y $p)
        #extra=$(guess_nonpilatus $y $p)

        for s in $scans $nonscans $extra; do
            sc_there=${s##*=}
            sc_here=${s%%=*}
            echo "CAMSYNC:    + Payload $y/$p / $sc_here <= $sc_there"
            do_sync "$sc_there" "$sc_here" "$y" "$p"
        done
    done
done
