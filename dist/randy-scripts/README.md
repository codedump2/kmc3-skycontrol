Scripts in use on the KMC3 FreeNAS / Fileserver
===============================================

- `sync_from_ancient_camserver`: Synchronisation of Pilatus image data
  (currently 10.0.0.114) onto the file server. Should be set up as
  a cronjob that runs once every few minutes (1-5 minutes).
  
- `sync_nas_to_udkm`: Two-way sync for specific data between local NAS
  and UDKM server, mostly for backup purposes, but also for remote access.
  Should run nightly.
  
- `sync_measurement_to_udkm`: One-way sync from KMC3 NAS to UDKM server
  for measurement data only.
  Should run nightly.
